const db = require('../libs/db.js');

module.exports = {
    async getUserById(id) {
        return await db.query("SELECT * FROM users WHERE id = ?", [id]);
    }
}
