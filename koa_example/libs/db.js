"use strict"

const mysql = require('mysql2/promise');

let pool;

async function connectDB() {
    pool = await mysql.createPool({
        connectionLimit: 10,
        host: 'localhost',
        user: 'root',
        database: 'db1'
    });
}

connectDB();

module.exports = {
    async query(sql, params) {
        const connection = await pool.getConnection();
        let [rows] = await connection.query(sql, params);
        connection.release();

        return rows;
    }
}