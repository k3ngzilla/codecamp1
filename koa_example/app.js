"use strict"

const Koa = require('koa');
const render = require('koa-ejs');
const path = require('path');
const app = new Koa();
const router = require('./routes');
const session = require('koa-session');
const db = require('./libs/db');
const formidable = require('koa2-formidable');

render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.keys = ['Some Random string'];

const sessionStore = {};
const CONFIG = {
    key: 'myName',
    maxAge: 1,
    httpOnly: true,
    /*store: {
        get (key, maxAge, { rolling }) {
            console.log(sessionStore);
            return sessionStore[key];
        },
        set (key, myValue, maxAge, { rolling, changed }) {
            console.log(sessionStore);
            sessionStore[key] = myValue;
        },
        destroy (key) {
            delete sessionStore[key];
        }
    }*/
}

app.use(session(CONFIG, app));
app.use(formidable());

app.use(async (ctx, next) => {
    ctx.myVariable = 'Hello World';
    ctx.db = db;

    await next();
});
app.on('session:missed', function (arg){
    console.log('missed');
});
app.on('session:invalid', function (arg){
    console.log('invalid');
});
app.on('session:expired', function (arg){
    console.log('expired');
});
app.use(router.routes());
app.listen(3000);