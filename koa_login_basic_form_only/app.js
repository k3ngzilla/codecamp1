const Koa = require('Koa');
const Router = require('koa-router');
const session = require('koa-session');
const render = require('koa-ejs');
const path = require('path');
const app = new Koa();
const router = new Router();
const formidable = require('koa2-formidable')

render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.keys = ['asdf;lkdsa naksldflkdsajflkdsa'];
app.use (formidable ());

router.get('/', async(ctx) => {
    let n = ctx.session.views || 0;
    let n2;
    
    if (ctx.session.myStupidVariable && ctx.session.myStupidVariable.length > 0)
        n2 = ctx.session.myStupidVariable.length;
    else {
        n2 = 0;
        ctx.session.myStupidVariable = [];
    }
    ctx.session.views = ++n;
    ctx.session.myStupidVariable.push(++n2);
    ctx.body = n + ' views | n2 ' + n2 + ' views | ' + JSON.stringify(ctx.session.myStupidVariable) ;
}).get('/test_view', async (ctx) => {
    await ctx.render('index');
}).post('/check_login', async (ctx) => {
    console.log(ctx.request.body.username);
    await ctx.render('check_login', {
        username: ctx.request.body.username
    });
});

const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    maxAge: 60 * 60 * 1000,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        }, 
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {

        }
    }
};
  
  app.use(session(CONFIG, app));

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);